//
//  SideMenuViewController.swift
//  JonkyKongTesting
//
//  Created by Antonín Charvát on 24/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    var menuItems = MenuItems.items
    var groupsOpen = false {
        didSet {
            if groupsOpen == false {
                menuItems = MenuItems.items
            }else{
                var tempMenuItems = MenuItems.items
                tempMenuItems.insertContentsOf(MenuItems.groups, at: 4)
                menuItems = tempMenuItems
            }
        }
    }
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        SideMenuManager.menuPresentMode = .MenuSlideIn
        SideMenuManager.menuFadeStatusBar = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: Controls
    func headerCellTapped(recognizer: UITapGestureRecognizer) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("ProfileViewController") as! ProfileViewController
        self.navigationController?.showViewController(vc, sender: self)
    }
}


// MARK: UITableViewDataSource
extension SideMenuViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuHeaderCell") as? MenuHeaderCell
        cell?.headerImage.image = MenuHeader.image
        cell?.headerName.text = MenuHeader.name
        cell?.headerPosition.text = MenuHeader.position
        cell?.tapRecognizer.addTarget(self, action: #selector(SideMenuViewController.headerCellTapped(_:)))
        return cell
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        
        if (indexPath.row > 3) && (indexPath.row < MenuItems.groups.count + 4) {
            if groupsOpen {
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuGroupCell") as? MenuGroupCell
                cell?.groupName.text = menuItems[indexPath.row]
                return cell!
            }
        }

        let cell = tableView.dequeueReusableCellWithIdentifier("MenuItemCell") as? MenuItemCell
        var imageIndex: Int {
            if groupsOpen {
                if indexPath.row > MenuItems.groups.count + 3 {
                    // when groups are open, we need to offset the indexPath of image
                    return (indexPath.row - MenuItems.groups.count)
                }
            }
            return indexPath.row
        }
        cell?.itemImage.image = MenuItems.itemsIcons[imageIndex]
        cell?.itemName.text = menuItems[indexPath.row]
        return cell!
    }
}


// MARK: UITableViewDelegate
extension SideMenuViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 3 {
            groupsOpen = !groupsOpen
            tableView.reloadData()
        }else{
            let vc = storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
            self.navigationController?.showViewController(vc, sender: self)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
}
